# Schema of an MyAcademicId Verifiable Credential

Draft notes and questions:

1. The names of the properties have mostly the same names as used here https://wiki.geant.org/display/SM/Attributes+available+to+Relying+Parties, so not stricly SAML or OIDC claim names.
2. Should "assurance" be made into "evidence"?
3. What to use for "id"?
